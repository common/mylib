#ifndef __MYLIB_LIB_ANOTHERCLASS__
#define __MYLIB_LIB_ANOTHERCLASS__

#include <mylib_Export.h>

class AnotherClass 
{

public: 

  float v;

  mylib_EXPORT void SetV( float& );

};

#endif
