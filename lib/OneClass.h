#ifndef __MYLIB_LIB_ONECLASS__
#define __MYLIB_LIB_ONECLASS__

#include <mylib_Export.h>

class OneClass 
{

public: 
  int x, y;

  mylib_EXPORT void SetXY( int&, int& );

};

#endif
