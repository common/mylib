

#include <OneClass.h>
#include <AnotherClass.h>


int main ( void )
{

  OneClass oneObject;
  AnotherClass anotherObject;

  int i = 1, j = 2;
  float f = 1.0f;

  oneObject.SetXY( i, j );
  anotherObject.SetV( f );

  return 0;

}
